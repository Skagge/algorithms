const int heap_capacity = 10;

class Heap {
private:
	int capacity = heap_capacity;
	int size = 0;
	int items[heap_capacity];

	int getLeftChildIndex(int parentIndex);
	int getRightChildIndex(int parentIndex);
	int getParentIndex(int childIndex);

	bool hasLeftChild(int index);
	bool hasRightChild(int index);
	bool hasParent(int index);

	int leftChild(int index);
	int rightChild(int index);
	int parent(int index);

	void swap(int indexOne, int indexTwo);

public:
	int peek();
	int poll();
	void add(int item);
	void heapifyUp();
	void heapifyDown();
	void heapify(int n, int i);
	void setRandomData();
	void heapsort();
	void show();
	bool test();
};