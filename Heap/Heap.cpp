#include "Heap.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;

int Heap::getLeftChildIndex(int parentIndex) {
	return 2 * parentIndex + 1;
}

int Heap::getRightChildIndex(int parentIndex) {
	return 2 * parentIndex + 2;
}

int Heap::getParentIndex(int childIndex) {
	return (childIndex - 1) / 2;
}

bool Heap::hasLeftChild(int index) {
	return getLeftChildIndex(index) < size;
}

bool Heap::hasRightChild(int index) {
	return getRightChildIndex(index) < size;
}

bool Heap::hasParent(int index) {
	return getParentIndex(index) >= 0;
}

int Heap::leftChild(int index) {
	return items[getLeftChildIndex(index)];
}

int Heap::rightChild(int index) {
	return items[getRightChildIndex(index)];
}

int Heap::parent(int index) {
	return items[getParentIndex(index)];
}

void Heap::swap(int indexOne, int indexTwo) {
	int temp = items[indexOne];
	items[indexOne] = items[indexTwo];
	items[indexTwo] = temp;
}

int Heap::peek() {
	if (size != 0) {
		return items[0];
	}
}

int Heap::poll() {
	if (size != 0) {
		int item = items[0];
		items[0] = items[size - 1];
		size--;
		heapifyDown();
		return item;
	}
}

void Heap::add(int item) {
	if (size < capacity) {
		items[size] = item;
		size++;
		heapifyUp();
	}
}

void Heap::heapifyUp() {
	int index = size - 1;
	while (hasParent(index) && parent(index) < items[index]) {
		swap(getParentIndex(index), index);
		index = getParentIndex(index);
	}
}

void Heap::heapifyDown() {
	int index = 0;
	while (hasLeftChild(index)) {
		int smallerChildIndex = getLeftChildIndex(index);

		if (hasRightChild(index) && rightChild(index) > leftChild(index)) {
			smallerChildIndex = getRightChildIndex(index);
		}

		if (items[index] > items[smallerChildIndex]) {
			break;
		}

		else {
			swap(index, smallerChildIndex);
		}

		index = smallerChildIndex;
	}
}

void Heap::setRandomData() {
	srand(time(NULL));

	for (int i = 0; i < heap_capacity; i++) {
		items[i] = 0;
	}

	for (int i = 0; i < heap_capacity; i++) {
		add(rand() % 100 + 1);
	}
}

void Heap::show() {
	for (int i = 0; i < size; i++) {
		cout << items[i] << " ";
	}

	cout << "\n\n";
}

bool Heap::test() {
	for (int i = 0; i < size - 1; i++) {
		if (items[i] > items[i + 1]) {
			cout << "Test zakonczony niepowodzeniem\n";
			return false;
		}
	}

	cout << "Test przebiegl pomyslne\n";
	return true;
}

void Heap::heapify(int n, int i) {
	int largest = i;
	int l = 2 * i + 1;
	int r = 2 * i + 2;

	if (l < n && items[l] > items[largest]) {
		largest = l;
	}

	if (r < n && items[r] > items[largest]) {
		largest = r;
	}

	if (largest != i) {
		swap(i, largest);
		heapify(n, largest);
	}
}

void Heap::heapsort() {
	for (int i = size - 1; i >= 0; i--) {
		cout << items[0] << " ";
		swap(0, i);
		heapify(i, 0);
	}
}