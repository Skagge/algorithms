const int array_size = 20;

class SortArray {
private:
	int array[array_size];

public:
	void setRandomData();
	void sort(int l, int r);
	void merge(int l, int m, int r);
	void show();
	bool test();
};
