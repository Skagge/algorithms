#include "SortArray.h"

int main() {

	SortArray example;

	example.setRandomData();
	example.show();
	example.sort(0, array_size-1);
	example.show();
	example.test();

	return 0;
}