#include "SortArray.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;

void SortArray::setRandomData() {
	srand(time(NULL));

	for (int i = 0; i < array_size; i++) {
		array[i] = rand() % 100 + 1;
	}
}

void SortArray::sort(int l, int r) {
	if (l < r) {
		int m = l + (r - l) / 2;
		sort(l, m);
		sort(m + 1, r);
		merge(l, m, r);
	}
}

void SortArray::merge(int l, int m, int r) {
	int n1 = m - l + 1;
	int n2 = r - m;

	int* arrayLeft = new int[n1];
	int* arrayRight = new int[n2];

	for (int i = 0; i < n1; i++) {
		arrayLeft[i] = array[l + i];
	}

	for (int i = 0; i < n2; i++) {
		arrayRight[i] = array[m + 1 + i];
	}

	int i = 0;
	int j = 0;
	int k = l;

	while (i < n1 && j < n2) {
		if (arrayLeft[i] <= arrayRight[j]) {
			array[k] = arrayLeft[i];
			i++;
		}
		else {
			array[k] = arrayRight[j];
			j++;
		}
		k++;
	}

	while (i < n1) {
		array[k] = arrayLeft[i];
		i++;
		k++;
	}

	while (j < n2) {
		array[k] = arrayRight[j];
		j++;
		k++;
	}

	delete[] arrayLeft;
	delete[] arrayRight;
}

void SortArray::show() {
	for (int i = 0; i < array_size; i++) {
		cout << array[i] << " ";
	}

	cout << "\n\n";
}

bool SortArray::test() {
	for (int i = 0; i < array_size - 1; i++) {
		if (array[i] > array[i + 1]) {
			cout << "Test zakonczony niepowodzeniem\n";
			return false;
		}
	}

	cout << "Test przebiegl pomyslnie\n";
	return true;
}