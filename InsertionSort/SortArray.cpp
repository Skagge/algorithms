﻿#include "SortArray.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;

void SortArray::setRandomData() {
	srand(time(NULL));
	
	for (int i = 0; i < array_size; i++) {
		array[i] = rand() % 100 + 1;
	}
}

void SortArray::sort() {
	for (int i = 1, element, j; i < array_size; i++) {
		element = array[i];
		j = i - 1;

		while (j >= 0 && array[j] > element) {
			array[j + 1] = array[j];
			j--;
		}

		array[j + 1] = element;
	}
}

void SortArray::show() {
	for (int i = 0; i < array_size; i++) {
		cout << array[i] << " ";
	}

	cout << "\n\n";
}

bool SortArray::test() {
	for (int i = 0; i < array_size-1; i++) {
		if (array[i] > array[i + 1]) {
			cout << "Test zakonczony niepowodzeniem\n";
			return false;
		}
	}

	cout << "Test przebiegl pomyslne\n";
	return true;
}