#include "SortArray.h"

int main() {

	SortArray example;

	example.setRandomData();
	example.show();
	example.sort();
	example.show();
	example.test();

	return 0;
}