const int array_size = 20;

class SortArray {
private:
	int array[array_size];

public:
	void setRandomData();
	void sort();
	void show();
	bool test();
};
